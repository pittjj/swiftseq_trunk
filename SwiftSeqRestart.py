#! /usr/bin/env python

import argparse, os, sys, subprocess, multiprocessing
from run import Run

def getHelpString():
	""" Default atring that should be printered out for exceptions. Will
	reference the wiki as well as the SwiftSeq mailing list"""
	
	helpStr = ('\n### Help ###\n'
				'Please carefully review the above exception\n'
				'For assistance running SwiftSeq, visit X. If your ' 
				'question remains unanswered, email us at Y.')
	return helpStr

def executeSwiftRestart(configDict, rlog):
	''' Note this is for 0.96... SWIFT_USERHOME needs to be in PATH 
	
	Does not tee output''' 
	
	os.system(configDict['swift'] + ' -resume ' + rlog + ' -config ' + configDict['conf'] + ' ' + configDict['swiftScript'])
	
def parseRestartConfig(configFP):
	''' Will return vars for swift, cf, xml, cdm, tc, swiftScript 
	
	Need more detailed error checking'''
	
	required = ['swift', 'conf', 'swiftScript']
	
	FH = open(configFP, 'r')
	
	configDict = {}
	for line in FH:
		temp = line.strip().split('=', 1)
		configDict[temp[0]] = temp[1]
		
	for requirement in required:
		if requirement not in configDict:
			raise Exception('The required argument %s is not found in config file %s' % (requirement, configFP.strip()))
			
	return configDict

###########################
# Parser helper functions
###########################
def isValidFile(parser, arg):
	""" Will check that files exists and will raise a parser error if it
	does not. This will not yield true for directories"""
	
	if not os.path.isfile(arg):
		parser.error(('The provided file ' + arg + ' does not exist.'
		+ getHelpString()))
	else:
		return arg
        
def isValidDir(parser, arg):
	""" Will check that the path exists (is a directory) and also be sure
	that it isn't a file. Will raise a parser error if it is not."""
	
	if not os.path.exists(arg) or os.path.isfile(arg):
			parser.error(('The provided argument ' + arg + ' is not a directory.\n'
			+ getHelpString()))
	else:
		return arg
	
def setEnvironmentVariables(varName, var):
	""" Set all variables for the current shell - Will be inherited by
	subprocesses"""
	
	os.environ[varName] = var
	
def getEnviromentException(varName):
	""" The exception string that will be generated and expose to the user
	if a required environmental variable cannot be found"""

##################################
# Argparse
##################################

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--rlog', required=True, dest='rlog', type=lambda x: isValidFile(parser, x),
	help='The SwiftSeq restart log (rlog). Will be used to determine and execute remaining tasks (Required).')
parser.add_argument('-c', '--config', required=True, dest='config', type=lambda x: isValidFile(parser, x),
	help='The SwiftSeq config log. Contains paths to config files required by Swift (Required).')
parser.add_argument('--heapMax', type=int, dest='heapMax', default=2500, nargs='?', const='all', 
	help='Java max heap size (in MB) for the Swift process running on the headnode (Default = 2500)')
parser.add_argument('--gcThreads', type=int, dest='gcThreads', default=1, nargs='?', const='all', 
	help='Number of threads dedicated for parallel garbage collection on the Swift process (Default = 1)')
	
args = parser.parse_args()

# Get SwiftSeq.py executable directory
# Maybe put this in a try-except block
swiftSeqDir = repr(os.path.dirname(os.path.realpath(sys.argv[0]))) + '/'
workdir = os.getcwd()
subprocess.call(swiftSeqDir +'/graphic.sh', shell=True)
print 'Preparing to restart SwiftSeq run...\n\n'
	
##################################
# Gather enviroment variables
##################################
# PATH will be passed to each wrapper
try:
	PATH = os.environ['PATH']
except KeyError:
	exception = getEnviromentException('PATH')
	raise Exception(exception)

####################
# Get config dict
####################
configDict = parseRestartConfig(args.config)


##################################
# Set enviroment variables
##################################
# SWIFT_HEAP_MAX
SWIFT_HEAP_MAX = '%sM' % (str(args.heapMax))
setEnvironmentVariables('SWIFT_HEAP_MAX', SWIFT_HEAP_MAX)
#COG_OPTS
COG_OPTS = '-XX:+UseParallelGC -XX:ParallelGCThreads=%s' % (str(args.gcThreads))
setEnvironmentVariables('COG_OPTS', COG_OPTS)

# SWIFT_HOME
setEnvironmentVariables('SWIFT_HOME', configDict['SWIFT_HOME'])
# SWIFT_USERHOME
setEnvironmentVariables('SWIFT_USERHOME', configDict['SWIFT_USERHOME'])


# Re-run SwiftSeq using the configs are rlog provided
executeSwiftRestart(configDict, args.rlog)
