import sys, json
from pprint import pprint

from findInputFiles import *
from appSync import *
from swiftConfig import *
from tumorNormalSwift import *
from germlineSwift import *
from parseWorkflow import *
from swiftConfigNew import *

""" This class is used to parse the various config files and saves them
as variables so they can be passed to various algorithms

Should also dump all of the information gathered here to a log file that can be 
used for various debugging purposes

Vars needed

"""

class Run:
	""" This class is used to parse the various config files (json, ref, and exe)
	 as well as PATH and saves them as variables so they can be passed to various algorithms"""
	
	def __init__(self, args, PATH, workDir, swiftSeqDir, LD_LIBRARY_PATH):
		
		# Will need to convert all args in parser oject to string
		#!# clean up this section
		
		self.exe = self.parseConfig(args.exeConfig)
		self.ref = self.parseConfig(args.refConfig)
		self.args = args
		self.dataDir = args.data
		self.workflowPath = args.workflowPath
		self.PATH = PATH
		self.LD_LIBRARY_PATH = LD_LIBRARY_PATH
		self.workDir = workDir
		self.swiftSeqDir = swiftSeqDir
		self.utilitiesDir = swiftSeqDir + 'utilities'
		self.wrapperDir = self.mkWrapperDir()
		self.tmp = args.tmp
		self.outDir = 'analysis'
		# hardcoded for now... should just leave as true
		self.lazyErrors = args.lazyErrors
		self.runName = args.runName
		self.project = args.project
		self.queue = args.queue
		self.jobOptions = args.jobOptions
		self.jobManager = args.jobManager
		self.maxCores = args.coresPerNode
		self.maxMem = args.ramPerNode
		
		if args.workerTmp:
			self.workerTmp = '/tmp/'
		else:
			self.workerTmp = ''
		
		################################################################
		# Parse config files
		################################################################
		# Will be returned as dictionaries
		self.exe = self.parseConfig(args.exeConfig)
		self.ref = self.parseConfig(args.refConfig)
		
		################################################################
		# Set up utilities & system params
		################################################################
		self.initializeUtilities()
		self.getSysSpecificParams(args)
		
		################################################################
		# parse workflow
		################################################################
		self.workflow = Workflow(self.workflowPath)
		self.runType = self.workflow.runType
		self.dataType = self.workflow.dataType
		
		
		################################################################
		# Gather input files
		################################################################
		print '---------------------------------------------------------'
		print 'Gathering input files and read group information...'
		print '---------------------------------------------------------'
		# Hardcoded '.bam' suffix
		if self.workflow.dataType == 'Tumor_Normal_Pair':
			inputFiles = InputFiles(self.workflow.dataType, self.dataDir, self.outDir, '.bam', self.exe['samtools'])
		else: # Will be germline
			inputFiles = InputFiles(self.workflow.dataType, self.dataDir, self.outDir, '.bam', self.exe['samtools'])
		
		#!# Hardcoded interval size for now... make variable later
		contigs = Contigs(self.ref['refDict'], self.args.genoSegmentSize, self.outDir, inputFiles.symlinkFiles)
		
		
		################################################################
		# Construct Swift syntax
		################################################################
		if self.workflow.dataType == 'Tumor_Normal_Pair':
			self.swiftScript = tumorNormalSwift(self.workflow, workDir, contigs.contigsFileFp, self.outDir)
		else: # Will be germline
			self.swiftScript = germlineSwift(self.workflow, workDir, contigs.contigsFileFp, self.outDir)
		
		
		''' This will be the last step '''
		################################################################
		# Construct wrappers & swift config
		################################################################
		self.constructWorkflowWrappers()
		#self.configureSwift = ConfigureSwift(self, tcStr)
		self.conf = SwiftConfigNew(self) # passing in run
	
	def parseConfig(self, filepath):
		"""Will parse the algorithm or reference config and return a dict
		connecting algorithm/reference names to filepaths. Will eventually
		need to do some exception handling here
		
		Could also check to be sure the path given is a file """
		
		configDict = {}
		FH = open(filepath, 'r')
		
		for line in FH:
			# if line is a comment or a blank line continue
			if line[0] == '#' or line.strip() == '':
				continue
			
			temp = line.strip().split('=')
			# Determine if the line is appropriately formated
			if len(temp) == 1 or len(temp) > 2:
				raise Exception('Reference config file (%s) is malformed' % (filepath))
		
			key, value = temp
			# Determine if key is recognized
			# Determine if value is the type we'd expect
			configDict[key] = value
			
		FH.close()
		return configDict
		
	
	def getSysSpecificParams(self, args):
		""" Will need to generate... gcFlag, javaMem, packingCores """
		
		self.ramPerJob = str(args.ramPerNode / args.tasksPerNode)
		# Will only be used for a couple of apps
		self.packingCores = str(args.coresPerNode / args.tasksPerNode)
		# May need to be refined for HaplotypeCaller - Just don't use for multi-threaded
		self.gcCores = (args.coresPerNode - args.tasksPerNode) / args.tasksPerNode
		
		# If number of cores available to do garbage collection <= 0 then
		# Do not perform parallel GC collection
		if self.gcCores <= 0:
			self.gcCores = 0
			self.gcFlag = ''
		else:
			self.gcFlag = '-XX:+UseParallelGC -XX:ParallelGCThreads=%s' % (str(self.gcCores))
			
		self.javaMem = '-Xmx%sm' % (str(self.ramPerJob))
		self.ramPoolMem = '-Xmx%sm' % (str(7500))
		
		# jobThrottle - Per David Kelly recommendation
		#self.jobThrottle = round((float(args.numNodes * args.tasksPerNode) / 100.0), 3)
		
	def mkWrapperDir(self):
		""" """
		wrapperDir = self.workDir + '/wrappers/'
		mkDir(wrapperDir)
		return wrapperDir
		
	def constructWorkflowWrappers(self):
		""" Recreate this having it interact with parseWorkflow
		
		Ideally
		
		tc entries depricated  """
		
		baseApps = getBaseApps()
		customApps = getCustomApps()
		
		# Base apps
		for app in baseApps:
			exclusion = baseApps[app]['exclusion']
			# Skip base app if the exclusion is true
			# Leave this for now as it will be useful
			if exclusion == self.runType:
				continue
			#appName = baseApps[app]['name'] # Might be redundant with app
			params = '' # no params need to be specified
			printWrapperFxn = baseApps[app]['print']
			printWrapperFxn(self, app, params)

		# Apps specified in the json workflow
		programsDict = self.workflow.programsDict
		for app in programsDict:
			params = programsDict[app]['params']
			walltime = programsDict[app]['walltime']
			printWrapperFxn = customApps[app]['print'] # custom apps
			printWrapperFxn(self, app, params)
			
			
	def initializeUtilities(self):
		"""Need to be sure these are visable from workers in cloud env
		
		These should already exist... this is just collecting the apths"""
		
		utilitiesDir = self.swiftSeqDir + 'utilities/'
		
		#grpReduce
		self.grpReduceDir = utilitiesDir + '/grpReduce'
		self.grpReduce = utilitiesDir + '/grpReduce/grpReduce'
		#DoC
		self.getDoC = utilitiesDir + 'getDoC.py'
		# need ones from alignment?
