from printWrappers import printFile
from appSync import *

### Add support of ENV variables!

class SwiftConfigNew:
	""" ddd """
	
	def __init__(self, run):
		""" All config files will be written to the workDir """
		
		#configHandle
		self.swiftConf = 'Swift.conf'
		self.FH = open(self.swiftConf, 'w')
		
		# get the apps that need to be written to conf - dicts - merges into one dict
		appsDict = getBaseApps().copy()
		appsDict.update(getCustomApps())
		
		# site is synonymous for pool in this case
		appsBySite = self.partitionAppsBySite(appsDict)
		
		# Can get this info from app dict... would be better to do so
		sites = appsBySite.keys()
		
		# write out sites
		self.FH.write('sites: %s\n\n' % (str(sites).replace("'","")))
		
		# SITE
		for site in sites:
			
			hostNumber = str(sites.index(site))
			
			# initialize for each site
			self.FH.write('site.%s {\n' % (site))
			
			# fill in executable info
			self.printExecutionSettings(1, run, hostNumber, site)
			
			# extra site info
			###!!!!!self.printConfigLines(1, run)
			
			# Deal with apps
			for appName in appsBySite[site]:
				appInfo = appsBySite[site][appName]
				wrapperPath = run.wrapperDir + appName + '.sh'
				walltime = appInfo['walltime']
				tmp = run.args.tmp
			
				self.printApp(1, run, appName, wrapperPath, walltime)
			
			# extra infor per site
			self.printConfigSiteLines(1, run)
				
			# close site
			self.FH.write('}\n\n')
		
		# extra config info
		self.printConfigLines(0, run)
		
		self.FH.close()

	def printApp(self, tabCount, run, appName, wrapperPath, walltime):
		''' ddd '''
		
		tabs = '\t' * tabCount
		
		Str = (tabs + 'app.' + appName + ' {\n' +
					tabs + '\texecutable: "' +  wrapperPath + '"\n' +
					tabs + '\tmaxWallTime: "' + walltime + '"\n' +
					tabs + '\tenv.TMPDIR="' + run.args.tmp + '"\n' +
				tabs + '}\n\n')
				
		self.FH.write(Str)
		
	def printConfigLines(self, tabCount, run):
		''' aaa '''
		
		tabs = '\t' * tabCount
		
		#tabs + 'tracingEnabled: true\n' +
			
		Str = (tabs + 'lazyErrors: ' + str(run.args.lazyErrors).lower() + '\n' +
				tabs + 'executionRetries: ' + str(run.args.retries) + '\n' +
				tabs + 'keepSiteDir: true\n' +
				tabs + 'logProvenance: false\n' +
				tabs + 'statusMode: "provider"\n' +
				tabs + 'providerStagingPinSwiftFiles: false\n' +
				tabs + 'alwaysTransferWrapperLog: false\n' +
				tabs + 'maxForeachThreads: 300\n' +
				tabs + 'TCPPortRange: "50000,51000"\n\n')
		
		self.FH.write(Str)
	
	def getJobOptions(self, tabStr, optionsLine):
		''' qqq '''
		
		Str = ''
		
		options = optionsLine.strip().split(';')
		for option in options:
			Str = Str + tabStr + option + '\n'
			
		return Str
		
	
	def printExecutionSettings(self, tabCount, run, hostNumber, site):
		''' ddd '''
		
		tabs = '\t' * tabCount
		
		if run.args.project != None:
			projectLine = tabs + ('\t' * 2) + 'jobProject: "' + run.args.project + '"\n'
		else:
			projectLine == ''
			
		loggingDir = run.workDir + '/' + 'workerLogging'
		
		if site == 'one':
			tasksPerNode = 1
		elif site == 'RAM':
			tasksPerNode = int(round(float(run.args.tasksPerNode / 2.0)))
		else:
			tasksPerNode = run.args.tasksPerNode
		
		
		Str = (tabs + 'execution {\n' +
					tabs + ('\t' * 1) + 'type: "coaster"\n' +
					tabs + ('\t' * 1) + 'URL: "localhost:' + str(hostNumber) + '"\n' +
					tabs + ('\t' * 1) + 'jobManager: "' + run.args.jobManager + '"\n' +
					tabs + ('\t' * 1) + 'options {\n' +
						tabs + ('\t' * 2) + 'nodeGranularity: 1\n' +
						tabs + ('\t' * 2) + 'jobQueue: "' + run.args.queue + '"\n' +
						tabs + ('\t' * 2) + 'maxNodesPerJob: 1\n' +
						tabs + ('\t' * 2) + 'maxJobs: ' + str(run.args.numNodes) + '\n' +
						tabs + ('\t' * 2) + 'highOverallocation: 100\n' +
						tabs + ('\t' * 2) + 'maxJobTime: "' + str(run.args.jobTime) + '"\n' +
						tabs + ('\t' * 2) + 'lowOverallocation: 100\n' +
						projectLine + 
						tabs + ('\t' * 2) + 'jobOptions {\n' +
							self.getJobOptions((tabs + ('\t' * 3)), run.args.jobOptions) + 
						tabs + ('\t' * 2) + '}\n' +
						tabs + ('\t' * 2) + 'tasksPerNode: ' + str(tasksPerNode) + '\n' +
						tabs + ('\t' * 2) + 'workerLoggingLevel: "DEBUG"\n' +
						tabs + ('\t' * 2) + 'workerLoggingDirectory: "' + loggingDir + '"\n' +
					tabs + ('\t' * 1) + '}\n' +
				tabs + '}\n\n')
				
		self.FH.write(Str)
	
	def printConfigSiteLines(self, tabCount, run):
		''' jjj'''
		
		tabs = '\t' * tabCount
		
		Str = (tabs + 'staging: direct\n' +
				tabs + 'workDirectory: "' + run.args.tmp + '"\n' +
				tabs + 'maxParallelTasks: 1001\n' +
				tabs + 'initialParallelTasks: 999\n\n')
				
		self.FH.write(Str)
	
	def partitionAppsBySite(self, appsDict):
		''' Will generate a a dict that has keys of sites and values will be
		info regarding the app ... note that pool is synonymous with app'''
		
		appsBySite = {}
		
		for app in appsDict:
			pool = appsDict[app]['pool']
			if pool not in appsBySite:
				appsBySite[pool] = {}
				appsBySite[pool][app] = appsDict[app]
			else:
				appsBySite[pool][app] = appsDict[app]
				
		return appsBySite
