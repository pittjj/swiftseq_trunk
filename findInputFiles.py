import os, sys, warnings
from os.path import commonprefix
from os.path import isfile
from subprocess import Popen, PIPE
import re

""" First just write this so it can find the appropriate files and make
the proper directories. Later, add sanity checks, ability to handle .RG
info, fastqs, etc. """

#########
# TO-DO
#########

# Need to handle if RG ID contains non-ascii
# Need to determine how Picard handles RG ID
# Should likely be writing results to some form of set-up log

#################
#### Classes ####
#################

class InputFiles:
	""" Add """
	def __init__(self, dataType, dataDir, analysisDir, suffix, samtools):
		
		#!# Are contigs needed here?
		self.patOutfileName = 'individuals.txt'
		self.sampleOutfileName = 'samples.txt'
		self.rgOutfileName = 'RGfiles.txt'
		self.rgIdOutfileName = 'RGIDs.txt'
		self.samtools = samtools
		
		# Input variables
		if dataDir[-1] != '/':
			self.dataDir = dataDir + '/'
		else:
			self.dataDir = dataDir
		
		if analysisDir[-1] != '/':
			self.analysisDir = analysisDir + '/'
		else:
			self.analysisDir = analysisDir
			
		self.suffix = suffix
		self.originalFiles = self.walkDir()
		
		# use and elif and have else do something else?
		if  dataType == 'Germline':
			self.germline()
			print 'Number of samples to be analyzed: %s\n\n' % (str(len(self.symlinkFiles)))
		else:
			self.tumorNormalPairs()
			print 'Number of individuals to be analyzed: %s' % (str(len(self.patientPaths)))
			print 'Number of samples (cancer and non-cancer) to be analyzed: %s\n\n' % (str(len(self.symlinkFiles)))

	
	def tumorNormalPairs(self):
		""" Pre-processing of tumor-normal samples """
		
		self.keepFiles = self.checkTumorNormalDirStructure(self.originalFiles)
		self.symlinkFiles = self.mkDirsSymLinks(self.keepFiles)
		self.inputFileDict = self.createTumNormDict(self.symlinkFiles)
		self.patientPaths = self.getPatientPaths()
		self.createRgFiles()
		self.createPatFile()
		self.createTissueFiles()
		self.createPairedOutputDirs()
		
	def germline(self):
		""" Pre-processing of germline samples """
		
		self.keepFiles = self.checkGermlineDirStructure(self.originalFiles)
		self.symlinkFiles = self.mkDirsSymLinks(self.keepFiles)
		self.inputFileDict = self.createGermlineDict(self.symlinkFiles)
		self.patientPaths = self.getPatientPaths()
		self.createRgFiles()
		self.createGermSampleFile()
	
	def walkDir(self):
		""" traverse the data dir to find files that end with suffix"""
		fileList = []
		for root, dirs, files in os.walk(self.dataDir, followlinks = True):
			relPath = root.replace(self.dataDir, '')
			
			if files == []:
				raise Exception(('No files found in the data directory. '\
				'Please check input files and data directory structure')
			
			for file_ in files:
				if file_.endswith(self.suffix):
					fileList.append(os.path.join(relPath, file_))
					
		return fileList
		
	def checkTumorNormalDirStructure(self, fileList):
		"""fff """
		keepFiles = []
		for file_ in fileList:
			topDirs = file_.split('/')[0:-1]
			# Should have a function that writes to a setup log
			if topDirs[-2] == 'tumor' or topDirs[-2] == 'normal':
				keepFiles.append(file_)
			else:
				warnings.warn('Warning: %s does not adhere to required directory structure. Will be ignored' % (file_))
		
		return keepFiles
		
	def checkGermlineDirStructure(self, fileList):
		""" Need to fix this """
		
		# Will be used to ensure only one file is present per dir path
		# This may need to be changed at some date
		dirPaths = set()
		keepFiles = []
		for file_ in fileList:
			topDirs = file_.split('/')[0:-1]
			dir_ = '/' + '/'.join(topDirs)
			# Should have a function that writes to a setup log
			if dir_ not in dirPaths:
				keepFiles.append(file_)
				dirPaths.add(dir_) 
			else:
				warnings.warn(('Warning: %s does not adhere to required directory' +
				'structure (multiple seq files in same directory). Will be ignored') % (file_))
		
		return keepFiles
		
	def mkDirsSymLinks(self, fileList):
		''' This function will take a list of files (that have been validated
		as files previously) and will create the approrpiated directory in the 
		analysisDir and will subsequently symlink the actual seq file in the
		analysis dir 
		
		Should check if file is actually the type of interest'''
		
		symlinkFiles = []
		for file_ in fileList:
			# Set the filepath in variables
			source = self.dataDir + file_
			destination = self.analysisDir + file_
			destinationPath = '/'.join(destination.split('/')[0:-1])
			# mk the directory and symlink the file
			mkDir(destinationPath)
			mkSymlink(source, destination)
			# Keep track of the symlinked file
			symlinkFiles.append(destination)
			
		return symlinkFiles
		
	def getPatientPaths(self):
		patientPaths = []
		for i in self.inputFileDict:
			patientPaths.append(i)
			
		return patientPaths

	def createTumNormDict(self, fileList):
		inputFileDict = {}
		for file_ in fileList:
			temp = file_.strip().split('/')
			ptPath = '/'.join(temp[0:-3]) + '/'
			tissue = temp[-3]
			sample = temp[-2]
			sampleFile = temp[-1]
			
			if ptPath not in inputFileDict:
				inputFileDict[ptPath] = {}
			if tissue not in inputFileDict[ptPath]:
				inputFileDict[ptPath][tissue] = {}
			if sample not in inputFileDict[ptPath][tissue]:
				inputFileDict[ptPath][tissue][sample] = {}
				
			inputFileDict[ptPath][tissue][sample] = sampleFile
			
		return inputFileDict
		
	def createGermlineDict(self, fileList):
		""" """
		
		inputFileDict = {}
		for file_ in fileList:
			temp = file_.strip().split('/')
			dirPath = '/'.join(temp[0:-1]) + '/'
			sampleFile = temp[-1]
			inputFileDict[dirPath] = sampleFile
			
		return inputFileDict
	
	# Should sort files prior to writing them out to add consistency
	# Which data files will this be?
	def createDataFiles(self, fileDict):
		pass
	
	def createPatFile(self):
		''' Use this file to create the analysis file for both germ and TN pair runs 
		
		currently only TN pair runs'''
		
		FH = open(self.analysisDir + self.patOutfileName, 'w')
		print >>FH, 'patient dir'
		for path in self.patientPaths:
			# Get patient name from dir path
			patientName = path.split('/')[-2]
			print >>FH, '%s %s' % (patientName, path)
		FH.close()
		
	def createGermSampleFile(self):
		''' This could be made more fluid in future version 
		
		COULD CLEAN NAMING CONVENTIONS'''
		
		FH = open(self.analysisDir + self.patOutfileName, 'w')
		print >>FH, 'ID dir'
		for path in self.patientPaths:
			# Get patient name from dir path
			ID = self.inputFileDict[path][0:-4]
			print >>FH, '%s %s' % (ID, path)
		FH.close()
		 
	def createTissueFiles(self):
		""" The tissue file will be used in TN pair runs to track multiple
		tumor or normal samples for that given tissue type """
		
		for patient in self.inputFileDict:
			for tissue in self.inputFileDict[patient]:
				dir_ = patient + tissue + '/'
				FH = open((dir_ + 'samples.txt'), 'w')
				print >>FH, 'ID sampleDir dir filepath'
				for sampleDir in self.inputFileDict[patient][tissue]:
					file_ = self.inputFileDict[patient][tissue][sampleDir]
					# IMPORTANT: Hardcoded for '.bam'
					ID = file_[0:-4]
					filepath = dir_ + sampleDir + '/' + file_
					print >>FH, '%s %s %s %s' % (ID, sampleDir, (dir_ + sampleDir), filepath)
			FH.close()
		return
		
	def getID(self, strList):
		""" Will find the greatest common substring in a list of strings.
		###
		May need to handle the case where no strings are common
		Will stip the suffix off at the end"""
		pass
		#return commonprefix(strList)
	
	# Will need to modify this to handle bams already split by RG
	def createRgFiles(self):
		""" fff """
		for file_ in self.symlinkFiles:
			dirPath = '/'.join(file_.split('/')[0:-1])
			readGroups = ReadGroups(file_, self.samtools)
			readGroupIds = readGroups.rgIds
			FH = open((dirPath + '/' + self.rgOutfileName), 'w')
			RGID = open((dirPath + '/' + self.rgIdOutfileName), 'w')
			
			for rg in readGroupIds:
				# Only bam
				rgFileName = file_[0:-4] + '.' + rg + '.bam'
				print >>FH, rgFileName
				print >>RGID, rg
			FH.close()
	
	def createPairedOutputDirs(self):
		""" ggf """
		
		# Collect dir names for each tumor and normal by patient
		for patient in self.inputFileDict:
			tumorSampleDirs = []
			normalSampleDirs = []
			for tumor in self.inputFileDict[patient]['tumor']:
				tumorSampleDirs.append(tumor)
			for normal in self.inputFileDict[patient]['normal']:
				normalSampleDirs.append(normal)
				
			# Make the paired analysis directories <tumorDirName>_<normalDirName>
			for tumorSample in tumorSampleDirs:
				for normalSample in normalSampleDirs:
					pairedDir = patient + 'pairedAnalyses/' + tumorSample + '___' + normalSample
					mkDir(pairedDir)
			

class Contigs:
	def __init__(self, refDict, intervalSize, analysisDir, symlinkFiles):
		self.refDict = refDict
		self.intervalSize = long(intervalSize)
		
		if analysisDir[-1] != '/':
			self.analysisDir = analysisDir + '/'
		else:
			self.analysisDir = analysisDir
			
		self.referenceDir = self.analysisDir + 'Reference/'
		
		# mk reference dir if it doesn't already exist
		mkDir(self.referenceDir)
		
		self.contigs, self.contigsFileFp = self.extractContigsFromDict(refDict, analysisDir)
		self.segmentGenomicRegions()
		
		for symlinkBam in symlinkFiles:
			self.mkContigsFile(symlinkBam, self.contigs)

	def extractContigsFromDict(self, refDict, analysisDir):
		""" Expects a Picard style dict for the reference file that will
		be used. Will parse that dict and write the contig names to a file
		in the reference directory """
		
		inDict = open(refDict, 'r')
		# Analysis dir should come in with tailing '/'
		contigsFileFp = self.referenceDir + 'contigs.txt'
		contigsFile = open(contigsFileFp, 'w')
		contigs = []

		for line in inDict:
			temp = line.strip().split('\t')
			if temp[0] == '@SQ':
				contig = temp[1].replace('SN:', '')
				print >>contigsFile, contig
				# So anything that uses contigs should have array elements
				# in the same order
				contigs.append(contig)
		print >>contigsFile, 'unmapped'
		contigsFile.close()
		
		return contigs, contigsFileFp
		
	def mkContigsFile(self, symlinkBam, contigs):
		""" Will create an expected contig file for each symlinked bam.
		The resulting file will be named ContigFiles.txt and will reside
		in the sample directory. Swift will use this file to know what
		The expected output of splits are. 
		
		NOTE: This function may be depricated in the latest version of
		SwiftSeq. Swift may only need to use the Contigs.txt file
		
		Now this function is being used again since bamtools needs it"""
		
		absOutDir = os.getcwd() + '/'

		# Assumes a bam as the input
		bamPrefix = symlinkBam[:-4].split('/')[-1] # strips off '.bam'
		bamDir = '/'.join(symlinkBam.split('/')[0:-1]) + '/'
		outFile = open(absOutDir + bamDir + 'sampleContigs.txt', 'w')
		for contig in contigs:
			contigBam = '%s%s.contig.%s.bam' % (absOutDir, bamDir + bamPrefix, contig)
			print >>outFile, contigBam
		# unmapped because this is what bamtools uses
		print >>outFile, '%s%s.contig.%s.bam' % (absOutDir, bamDir + bamPrefix, 'unampped')
		outFile.close()
		
	def segmentGenomicRegions(self):
		""" g """
		inDict = open(self.refDict, 'r')

		for record in inDict:
			temp = record.strip().split()
	
			# Skip the header line
			if temp[0] == '@HD':
				continue
	
			contig = temp[1].replace('SN:', '')
			endCoord = long(temp[2].replace('LN:', ''))
	
			# Open file specific to contig
			FH = open(((self.referenceDir + 'contig_segments_%s.txt') % (contig)), 'w')
	
			posMarker = 1
			while posMarker < endCoord:
				segStart = posMarker
				segEnd = posMarker + self.intervalSize
		
				if segEnd > endCoord:
					segEnd = endCoord
			
				print >>FH, '%s-%s' % (str(segStart), str(segEnd))
				posMarker = segEnd + 1
			FH.close()

		# initialize the unknown (unmapped) chr file
		FH = open((self.referenceDir + 'contig_segments_unmapped.txt'), 'w')
		print >>FH, 'no_mapped_reads'
		FH.close()

			
class ReadGroups:
	def __init__(self, bam, samtoolsPath):
		self.bam = bam
		self.samtoolsPath = samtoolsPath
		self.readGroupStrings = self.extractReadGroupStrings()
		self.rgById, self.rgIds = self.makeReadGroupDict()
		
	def extractReadGroupStrings(self):
		''' rrr '''
		
		readGroupStrings = []
		(stdout, stderr) = Popen([self.samtoolsPath, \
		 "view", "-H", self.bam,"|", "grep", "'@RG'"], stdout=PIPE).communicate()
		try:
			for line in stdout.split('\n'):                                                                                                                                      
				if line[0:3] == '@RG':
					rg = line.strip().replace(' ', '_')
					readGroupStrings.append(rg)
		except TypeError: # determine when this would return NA and if 'NA
			pass
			
		return readGroupStrings

	def makeReadGroupDict(self):
		''' www '''
		
		rgById = {}
		rgIds = []
		# Be sure this regex is functioning as expected
		IDregex = re.compile(r'ID:(.*?)(\t|$)')
		for rg in self.readGroupStrings:
			ID = IDregex.findall(rg)[0][0]
			# May need to catch if there are two or more IDs in the bam
			rgById[ID] = rg
			rgIds.append(ID)
			
		return rgById, rgIds

	def extractSM(self, readGroupString):
		''' Get the SM from the read group string
		
		If there is no SM, need to decide what to do... could have that file ignored..
		This is likely the best option since a file without RG ID will be ignored
		as well '''
		
		SMregex = re.compile(r'SM:(.*?)(\t|$)')
		# Need to handle when one isn't found or if multiples
		try:
			SM = SMregex.findall(readGroupString)[0]
			if len(SM) > 1:
				warnings.warn(('Warning: one or more read groups in %s have multiple SM fields .') % (file_))
		except IndexError:
			warnings.warn(('Warning: %s does not contain an SM field in its read group.') % (file_))
			# Need to make sure it is actually ignored
			# For TN pairs, the entire pair will need ot be ignored
			
		return SM
	
	def uniformSMfields(self):
		''' ttt '''
		
		SMs = set()
		for readGroupString in self.readGroupStrings:
			SM = self.extractSM(readGroupString)
			SMs.add(SM)
			
		return (len(SMs) == 1)


###################
#### Functions ####
###################

def mkDir(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)
		
def mkSymlink(source, destination):
	if not os.path.exists(destination):
		os.symlink(source, destination)


'''
######################################
##### Take command line arguments ####
######################################
#Needs to be a full filepath for now
dataDir = sys.argv[1]
analysisDir = sys.argv[2]
suffix = sys.argv[3]
samtools = sys.argv[4]
refDict = sys.argv[5]
intervalSize = sys.argv[6]

Y = InputFiles(dataDir, analysisDir, suffix, samtools)
Contigs(refDict, intervalSize, analysisDir, Y.symlinkFiles)
'''
