from printWrappers import printFile

""" This script will be used to print various swift configuration files """

# To-do
# The tc file needs to connect to the json file from the GUI
# Will need "standing" tc entries in conjunction wiht the ones determined
# by the json file

class ConfigureSwift:
	""" ddd """
	
	def __init__(self, run, tcStr):
		""" All config files will be written to the workDir """
		
		prefix = run.runName
		
		# Will contruct all the appropriate Swift file
		# SwiftSeq runs will use the runName to name these files
		self.cf = self.printCf(run, (prefix + '.cf') )
		self.tc = self.printTc(run, (prefix + '.tc'), tcStr)
		self.xml = self.printXml(run, (prefix + '.xml') )
		self.cdm = self.printCdm(run, (prefix + '.cdm') )
		

	def printCf(self, run, filename):
		"""fff"""
		
		Str = ('# Whether to transfer the wrappers from the compute nodes\n'
				'wrapperlog.always.transfer=false\n'
				'use.wrapper.staging=false\n\n'
	
				'# Indicates whether the working directory is on the remote site\n'
				'# should be left intact even when a run completes successfully\n'
				'sitedir.keep=true\n\n'
	
				'# try more times if fails\n'
				'execution.retries=' + str(run.args.retries) + '\n\n'
	
				'# Attempt to run as much as possible, i.g., ignore non-fatal errors\n'
				'lazy.errors=' + run.lazyErrors + '\n\n'
	
				'# to reduce filesystem access\n'
				'status.mode=provider\n'
				'use.provider.staging=false\n'
				'provider.staging.pin.swiftfiles=false\n'
				'foreach.max.threads=' + str(run.args.foreachMax) + '\n\n'
				'provenance.log=false\n')
		
		printFile(run.workDir, filename, Str, False)
		return (run.workDir + '/' + filename)
		
		
	def printTc(self, run, filename, tcStr):
		"""Not sure customStr should be passed in this instance...
		
		I think it would be better to call a function to get the custon string."""
		
		Str = ('# sitename  transformation path\n'
				'primary  echo  /bin/echo\n'
				'primary  cat  /bin/cat\n'
				'primary  ls  /bin/ls\n'
				'primary  grep  /bin/grep\n'
				'primary  sort  /bin/sort\n'
				'primary  paste  /bin/paste\n'
				'primary  scp  /bin/cp\n'
				'primary  touch  /bin/touch\n'
				'primary  wc  /usr/bin/wc\n\n'
				
				'# Entries for wrappers\n'
				+ tcStr)
				
		printFile(run.workDir, filename, Str, False)
		return (run.workDir + '/' + filename)
	
		
	def printXml(self, run, filename):
		"""Eventually write this to handle an arbitrary number of pools
		Will need to make the execution providers more general"""
		
		#!# may have to create the work directory!!!
		
		# project line
		if run.project != None:
			projectLine = self.getProjectLine(run.project)
		else:
			projectLine = ''
			
		# queue line
		if run.queue != None:
			queueLine = self.getQueueLine(run.queue)
		else:
			queueLine = ''
			
		# provider attributes line
		if run.providerAttributes != None:
			providerAttributesLine = self.getProviderAttributesLine(run.providerAttributes)
		else:
			providerAttributesLine = ''	
		
		# Changed this to be static for now... may change later or not be used at all
		jobThrottle = 10
		
		Str = ('<config>\n'
					'\t<pool handle="primary">\n'
						'\t\t<execution provider="coaster" jobManager="local:' + run.jobManager + '" url="localhost:1" />\n'
					    '\t\t<!-- replace with your project -->\n'
					    '\t\t' + projectLine + '\n'
					    '\t\t' + providerAttributesLine + '\n'
					    '\t\t' + queueLine +'\n'
					
					    '\t\t<profile namespace="globus" key="jobsPerNode">'+ str(run.args.tasksPerNode) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="maxTime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="maxwalltime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="lowOverallocation">100</profile>\n'
					    '\t\t<profile namespace="globus" key="highOverallocation">100</profile>\n\n'
					
					    '\t\t<profile namespace="globus" key="slots">' + str(run.args.numNodes) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="nodeGranularity">1</profile>\n'
					    '\t\t<profile namespace="globus" key="maxNodes">1</profile>\n'
					
					    '\t\t<profile namespace="karajan" key="jobThrottle">' + str(jobThrottle) + '</profile>\n'
					    '\t\t<profile namespace="karajan" key="initialScore">10000</profile>\n'
					
					    '\t\t<filesystem provider="local"/>\n'
					    '\t\t<workdirectory>' + run.args.tmp + '/swiftworkdir/</workdirectory>\n'
					'\t</pool>\n'
	
					'\t<pool handle="one">\n'
						'\t\t<execution provider="coaster" jobManager="local:' + run.jobManager + '" url="localhost:2" />\n'
					    '\t\t<!-- replace with your project -->\n'
					    '\t\t' + projectLine + '\n'
					    '\t\t' + providerAttributesLine + '\n'
					    '\t\t' + queueLine +'\n'
					
					    '\t\t<profile namespace="globus" key="jobsPerNode">1</profile>\n'
					    '\t\t<profile namespace="globus" key="maxTime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="maxwalltime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="lowOverallocation">100</profile>\n'
					    '\t\t<profile namespace="globus" key="highOverallocation">100</profile>\n\n'
					
					    '\t\t<profile namespace="globus" key="slots">' + str(run.args.numNodes) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="nodeGranularity">1</profile>\n'
					    '\t\t<profile namespace="globus" key="maxNodes">1</profile>\n'
					
					    '\t\t<profile namespace="karajan" key="jobThrottle">' + str(jobThrottle) + '</profile>\n'
					    '\t\t<profile namespace="karajan" key="initialScore">10000</profile>\n'
					
					    '\t\t<filesystem provider="local"/>\n'
					    '\t\t<workdirectory>' + run.args.tmp + '/swiftworkdir/</workdirectory>\n'
					'\t</pool>\n'
	
					'\t<pool handle="RAM">\n'
						'\t\t<execution provider="coaster" jobManager="local:' + run.jobManager + '" url="localhost:3" />\n'
					    '\t\t<!-- replace with your project -->\n'
					    '\t\t' + projectLine + '\n'
					    '\t\t' + providerAttributesLine + '\n'
					    '\t\t' + queueLine +'\n'
					
					    '\t\t<profile namespace="globus" key="jobsPerNode">2</profile>\n'
					    '\t\t<profile namespace="globus" key="maxTime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="maxwalltime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="lowOverallocation">100</profile>\n'
					    '\t\t<profile namespace="globus" key="highOverallocation">100</profile>\n\n'
					
					    '\t\t<profile namespace="globus" key="slots">' + str(run.args.numNodes) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="nodeGranularity">1</profile>\n'
					    '\t\t<profile namespace="globus" key="maxNodes">1</profile>\n'
					
					    '\t\t<profile namespace="karajan" key="jobThrottle">' + str(jobThrottle) + '</profile>\n'
					    '\t\t<profile namespace="karajan" key="initialScore">10000</profile>\n'
					
					    '\t\t<filesystem provider="local"/>\n'
					    '\t\t<workdirectory>' + run.args.tmp + '/swiftworkdir/</workdirectory>\n'
					'\t</pool>\n'
	
					'\t<pool handle="IO">\n'
						'\t\t<execution provider="coaster" jobManager="local:' + run.jobManager + '" url="localhost:4" />\n'
					    '\t\t<!-- replace with your project -->\n'
					    '\t\t' + projectLine + '\n'
					    '\t\t' + providerAttributesLine + '\n'
					    '\t\t' + queueLine +'\n'
					
					    '\t\t<profile namespace="globus" key="jobsPerNode">'+ str(run.args.tasksPerNode) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="maxTime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="maxwalltime">' + str(run.args.coasterTime) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="lowOverallocation">100</profile>\n'
					    '\t\t<profile namespace="globus" key="highOverallocation">100</profile>\n\n'
					
					    '\t\t<profile namespace="globus" key="slots">' + str(run.args.maxIO / run.args.tasksPerNode) + '</profile>\n'
					    '\t\t<profile namespace="globus" key="nodeGranularity">1</profile>\n'
					    '\t\t<profile namespace="globus" key="maxNodes">1</profile>\n'
					
					    '\t\t<profile namespace="karajan" key="jobThrottle">' + str(jobThrottle) + '</profile>\n'
					    '\t\t<profile namespace="karajan" key="initialScore">10000</profile>\n'
					
					    '\t\t<filesystem provider="local"/>\n'
					    '\t\t<workdirectory>' + run.args.tmp + '/swiftworkdir/</workdirectory>\n'
					'\t</pool>\n'
	
				'</config>\n')
				
		printFile(run.workDir, filename, Str, False)
		return (run.workDir + '/' + filename)
	
	def printCdm(self, run, filename):
		"""This prints the cdm file with the specific set of file movement rules
		SwiftSeq runs will use. Any new filetypes will need their extension added 
		here
		
		This will also be variable when running on clouds vs shared file systems"""
		
		Str = ('# Describe CDM for my job\n'
				'# all files are left where they are, the script will\n' 
				'# move them to tmp or use them where they are\n'
				'# true both for input and output\n'
				'#rule .*' + run.outDir + '\/.*txt DIRECT ' + run.workDir + '\n'
				'#rule .*' + run.outDir + '\/.*bam DIRECT ' + run.workDir + '\n'
				'#rule .*' + run.outDir + '\/.*bai DIRECT ' + run.workDir + '\n'
				'#rule .*' + run.outDir + '\/.*log DIRECT ' + run.workDir + '\n'
				'#rule .*' + run.outDir + '\/.*grp DIRECT ' + run.workDir + '\n'
				'#rule .*' + run.outDir + '\/.*metrics DIRECT ' + run.workDir + '\n'
				'#rule .*' + run.outDir + '\/.*vcf DIRECT ' + run.workDir + '\n'
				'#rule .*' + run.outDir + '\/.*flagstat DIRECT ' + run.workDir + '\n'
				'#rule .* DEFAULT')
	
		printFile(run.workDir, filename, Str, False)
		return (run.workDir + '/' + filename)


	def aggregateTcEntires():
		""" THis function will take some struct that contains the wrapper name,
		the filepath, the tmpdir, and the walltime. From this information it will
		call the getCustomTcEntryFunction and aggregate the strings. Once aggregated,
		that string will be appended to the tc file"""
	
		return ''
		
	def getProjectLine(self, projectId):
		''' gggg '''
		
		Str = '<profile namespace="globus" key="project">' + projectId + '</profile>'
	
		return Str
		
	def getQueueLine(self, queue):
		''' gggg '''
		
		Str = '<profile namespace="globus" key="queue">' + queue + '</profile>'
	
		return Str
		
	def getProviderAttributesLine(self, providerAttributes):
		''' For Beagle, the required attributes are "pbs.aprun;pbs.mpp;depth=24" '''
		
		Str = '<profile namespace="globus" key="providerAttributes">' + providerAttributes + '</profile>'
	
		return Str
		
#############################
# Function outside of class
#############################
def getCustomTcEntry(pool, appName, wrapperDir, tmp, walltime):
	"""This function will need to be called explicitly by the json"""
		
	wrapperPath = wrapperDir + appName + '.sh'
	tcEntry = (pool + ' ' + appName + ' ' + wrapperPath + 
		' INSTALLED  AMD64::LINUX  ENV::TMP=' + tmp + ';GLOBUS::'
		'maxwalltime="' + walltime + '"\n')
		
	return tcEntry
