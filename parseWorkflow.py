import sys, json
from pprint import pprint
from possibleApps import *

## To-do
# more detailed error checking of aligner, genotypers, etc.
# Look for incompatibilities in the json file

# to avoid error... need a better solution at some point
def getHelpString():
	""" Default atring that should be printered out for exceptions. Will
	reference the wiki as well as the SwiftSeq mailing list"""
	
	helpStr = ('\n### Help ###\n'
				'Please carefully review the above exception\n'
				'For assistance running SwiftSeq, visit X. If your ' 
				'question remains unanswered, email us at Y.')
	return helpStr


class Workflow:
	def __init__(self, jsonWorkflow):
		'''Needs to contain the following variables
		
		runType, dataType, genotypers, structVarCallers, gatk, rmDup '''
		
		# Open json workflow and parse into dict
		workflowDict = json.load(open(jsonWorkflow))
		# dump workflow for debugging
		print '---------------------------------------------------------'
		print 'Executing the following workflow...'
		print '---------------------------------------------------------'
		pprint(workflowDict)
		print '\n\n'
		
		# Get workflow specifics - error checking will be performed
		self.runType = self.getTypes(workflowDict, 'runType')
		self.dataType = self.getTypes(workflowDict, 'dataType')
		
		# These will be empty if either aren't present
		self.genotypers = self.getCallers(workflowDict, 'Genotyper')
		self.structVarCallers = self.getCallers(workflowDict, 'Structural_Variant_Caller')
		self. bamQualityControlApps = self.getBamQualityControlApps(workflowDict, 'Bam_Quality_Control')
		
		self.gatk = self.checkGatk(workflowDict, 'Gatk_Post-processing')
		self.rmDup = self.checkRmDup(workflowDict, 'Duplicate_Removal')
		
		self.setAlignGenoFlags() # will return alignment and genotyping bools
		if self.structVarCallers != []:
			self.structVars = True
		else:
			self.structVars = False
		
		# will be set above
		if self.alignment:
			self.aligner = self.getAligner(workflowDict)
		else:
			self.aligner = None

		# All programs in json that need to be printed with params will
		# be in this var
		self.programsDict = self.getAllPrograms(workflowDict)

	def checkJsonStructure(self):
		'''Iterate through all fields of the json. Use possibleApps.py to
		determine if all fields are valid
		
		Go all the way down to applications... be sure all fields in params
		& walltime dict are strings. And be sure walltime dict only has len == 1 '''
		
		pass
		# Implement this soon... may make some of the rules implemented in
		# functions below redundant 
	
	
	def getAllPrograms(self, workflowDict):
		'''All will be three deep in the tree
		
		Format param string and get walltime'''
		
		# Get all program types so all possible programs are caught
		allProgramTypes = possibleProgramTypes()
		javaPrograms = allJavaPrograms()
		programsDict = {}
		
		# iterate over program types
		for possibleProgramType in allProgramTypes:
			try:
				workflowDict[possibleProgramType]
			except KeyError: # Program type is not present
				continue
			
			# get all programs for each program type
			for program in workflowDict[possibleProgramType]:
				temp = workflowDict[possibleProgramType][program]
				# initialize dict
				programsDict[program] = {}
				# aquire walltime
				walltime = temp['walltime']
				programsDict[program]['walltime'] = walltime
				# ensure correct sep for java program params
				if program in javaPrograms:
					sep = '='
				else:
					sep = ' '
				
				# compile all params as a string
				# try except because if params not specified through GUI
				# the param string will not be present
				
				# Strelka will be kept as a dict of param : value
				# this is because Strelka requires param in a config and not
				# passed via the command line
				if program == 'Strelka':
					programsDict[program]['params'] = temp['params']
					continue
				
				try:
					paramsStr = self.paramsToString(temp['params'], sep)
				except KeyError:
					paramsStr = ''
				programsDict[program]['params'] = paramsStr
				
		return programsDict
			
	
	def setAlignGenoFlags(self):
		''' '''
		
		if self.runType == 'Processing_and_Genotyping':
			self.alignment = True
			self.genotyping = True
		elif self.runType == 'Processing':
			self.alignment = True
			self.genotyping = False
		elif self.runType == 'Genotyping':
			self.alignment = False
			self.genotyping = True
		else:
			raise Exception(('Workflow JSON file is malformed. ' 
						'Contains an unrecognized runType field\n' + getHelpString()))
			
	def getAligner(self, workflowDict):
		''' ddd '''
		
		possibleAligners = getPossibleAligners()
		
		# maybe check aligner dict length here
		
		try:
			aligner = workflowDict['Aligner'].keys()[0] # assumes only one aligner in file
		except:
			raise Exception(('Workflow JSON file is malformed. ' 
						'Processing specified but no aligner declared\n' + getHelpString()))
		
		if aligner in possibleAligners:
			return aligner
		else:
			raise Exception(('Workflow JSON file is malformed. ' 
						'Aligner name specified is not supported\n' + getHelpString()))
	
	def getCallers(self, workflowDict, callerType):
		''' Will take the entire json and will determine what variant callers
		are present. Will be returned as a list. If none of caller type present
		in workflow, a empty list will be returned'''
		
		callers = []
		
		try:
			workflowDict[callerType]
		except KeyError:
			return callers # empty list
		
		# Iterate over all variant callers & append
		for caller in workflowDict[callerType]:
			callers.append(caller)
			
		return callers
		
		
	def getBamQualityControlApps(self, workflowDict, appType):
		''' Will take the entire json and will determine what bam QC apps
		are present. Will be returned as a list. If no apps are present
		in workflow, a empty list will be returned'''
		
		bamQualityControlApps = []
		
		try:
			workflowDict[appType]
		except KeyError:
			return bamQualityControlApps # empty list
		
		# Iterate over all variant callers & append
		for app in workflowDict[appType]:
			bamQualityControlApps.append(app)
			
		return bamQualityControlApps

		
	def getTypes(self, workflowDict, typeStr):
		'''Should raise an exception if run or datatype not specified'''
		
		try:
			return workflowDict[typeStr]
		except KeyError:
			raise Exception(('Workflow JSON file is malformed. Does not contain' +
				typeStr + '\n' + getHelpString()))
	
				
	def checkGatk(self, workflowDict, gatkStr):
		'''Should raise an exception if run or datatype not specified'''
		
		try:
			# gatk present and indel realing and BQSR present as programs
			if workflowDict[gatkStr] and self.properGatkPrograms(workflowDict[gatkStr]):
				return True
			else:
				raise Exception(('Workflow JSON file is malformed. ' 
						'Does not contain proper gatk programs\n' + getHelpString()))
				
		except KeyError:
				return False # gatk not performed


	def properGatkPrograms(self, gatkDict):
		''' Be sure specific gatk algorithms present '''
		
		try:
			return (gatkDict['GatkIndelRealignment'] and gatkDict['GatkBqsr'])
		except:
			return False # if not dict, 'default' present, etc... checkGatk will raise exception
			
			
	def checkRmDup(self, workflowDict, rmDupStr):
		'''Should raise an exception if run or datatype not specified'''
		
		try:
			# gatk present and indel realing and BQSR present as programs
			if workflowDict[rmDupStr] and self.properRmDupPrograms(workflowDict[rmDupStr]):
				return True
			else:
				raise Exception(('Workflow JSON file is malformed. ' 
						'Does not contain a proper remove duplicates program\n' 
						+ getHelpString()))
				
		except KeyError:
				return False # rmDup not found in workflow & not performed


	def properRmDupPrograms(self, rmDupDict):
		''' Be sure specific rmDup program present '''
		
		if rmDupDict == 'default': # if program not specified
			return False
		elif len(rmDupDict) > 1: # if more than 1
			raise Exception(('Workflow JSON file is malformed. ' 
						'Contains > 1 remove duplicates program\n'
						+ getHelpString()))
						
		# get all possible rmDupPrograms
		rmDupPrograms = possibleRmDup()
		
		try:
			rmDupProgram = rmDupDict.keys()[0]
		except (AttributeError, IndexError):
			return False # in case rmDupProgram is not a dict or empty dict
		
		# Finally test if program name is a possible program
		if rmDupProgram in rmDupPrograms:
			return True
		else: 
			return False
		
	
	def paramsToString(self, paramDict, sep):
		""" Take a dictionary of parameters and returns a space separated
		string that will be passed to the appropriate wrapper
		
		Should maybe allow the type of separater to be specified as
		a param... think Java and = (e.g. Picard)"""
		
		paramStr = ''
		
		# Extract all params, values, and append to string
		for param in paramDict:
			value = paramDict[param]
			paramStr = paramStr + param + sep + value + ' '
			
		return paramStr
