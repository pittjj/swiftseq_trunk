#######################################
# variables needed for the wrappers
#######################################
"""
tmp
samtools
java
gcFlag
javaMem
gatk
ref
indelsMills
indels1kg
dbSnpVcf
gatkQueue
grpReduceDir
grpReduce
bamUtil
novosort
markDuplicates
genomeCoverageBed
python
getDoC
maxCores
maxMem
delly
snpEffDataDir
snpEff
snpEffBuild
snpEffConfig
bam2fastq
bwa
platypus
packingCores
"""

##################################
# reference files
##################################
# Some of these will be assumed to be in a directory with the index
ref
indelsMills
indels1kg
dbSnpVcf
snpEffDataDir
snpEffBuild
snpEffConfig

##################################
# executables
##################################
# algorithms
samtools
gatk
gatkQueue
bamUtil
novosort
markDuplicates
genomeCoverageBed
delly
snpEff
bam2fastq
bwa
platypus

# languages
java
python

##################################
# parameters
##################################
tmp
gcFlag
javaMem
maxCores
maxMem
packingCores

##################################
# Under-the-hood
##################################
grpReduceDir
grpReduce
getDoC
