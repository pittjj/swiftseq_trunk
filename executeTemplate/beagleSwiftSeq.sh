dataDir=$1

# Swift does not play well with IBM java
module load java

# NEED TO SPECIFY TMP HERE
export TMPDIR=<PATH_TO_SOME_TMP_DIR>
export SWIFT_HOME=/lustre/beagle/pittjj/software/swift-0.96-RC1
export LD_LIBRARY_PATH=/opt/gcc/4.9.1/snos/lib64:/soft/SuiteSparse/gnu/3.6.0/lib:/soft/metis/gnu/4.0.1/lib:/soft/cblas/gnu/3.0/lib:/soft/boost/gnu/1.48.0/lib:/soft/SuiteSparse/gnu/3.6.0/lib:/soft/metis/gnu/4.0.1/lib:/soft/cblas/gnu/3.0/lib:/soft/python/2.7/2.7.6-vanilla/python/lib:/opt/gcc/4.9.1/snos/lib64:/soft/torque/2.5.7/lib:/soft/ci/lib
export USER_HOME=/lustre/beagle/$LOGNAME
export SWIFT_USERHOME=${USER_HOME}
# Below will only work with users with White lab disk access
export HOME=/lustre/beagle/$LOGNAME

# NEED TO SPECIFY PATHS HERE
python <PATH_TO_SWIFTSEQ> -d $dataDir -n <INT_NODES_PER_SITE> --tmp <PATH_TO_TMP_DIR> \ 
-w <PATH_TO_WORKFLOW_JSON>  --refConfig <PATH_TO_REF_CONFIG> --exeConfig <PATH_TO_EXECUTABLE_CONFIG> \
 --queue batch --jobOptions 'pbs.aprun: true;pbs.mpp: true;depth: 32' --jobManager local:pbs 
 --project CI-MCB000145 --jobTime 48:00:00 -c 32 -r 62000 --retries 2 -t 12
 
 
 # As it sits, this will run using the standard queue
# If we had a reservation called 'cga.2994' the argument to --jobOptions
# would be 'pbs.aprun: true;pbs.mpp: true;depth: 32; pbs.resource_list: "advres=cga.2994"'
