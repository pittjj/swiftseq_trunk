from printWrappers import *

#!# Iterate through all Swift Apps
#!# try-except if the apps is present in the workflow
#!# if so, print the app after setting paramStr and walltime
#!# With an except just print out the wrapper and write to the tc file

#!# set up a mechanism to be sure the walltime of an app is not > the walltime
#!# of its coaster pool


def getCustomApps():
	""" All apps that are included in the JSON need to be included here 
	
	Default walltimes and pools are added for each of the apps
	
	We could try to make this more automated in the future... well also
	lead to potential versioning issues between the SwiftSeq code and the
	GUI itself"""
	
	#'<JSON ID>' : [<App/wrappper name>, <printWrapperFunction>
	# Could add print here
	# snpEff is currently not printed
	
	d = {'PicardMarkDuplicates' : {'name' : 'PicardMarkDuplicates', 'print' : printPicardMarkDuplicates, 'walltime' : '15:00:00', 'pool' : 'primary'},
		'GatkIndelRealignment' : {'name' : 'GatkIndelRealnment', 'print' : printGatkIndelRealnment, 'walltime' : '24:00:00', 'pool' : 'primary'},
		'GatkBqsr' : {'name' : 'GatkBqsr', 'print' : printGatkBqsr, 'walltime' : '24:00:00', 'pool' : 'primary'},
		'DellyGerm' : {'name' : 'DellyGerm', 'print' : printDellyGerm, 'walltime' : '18:00:00', 'pool' : 'primary'},
		'DellyPaired' : {'name' : 'DellyPaired', 'print' : printDellyPaired, 'walltime' : '18:00:00', 'pool' : 'primary'},
		'SnpEff' : {'name' : 'SnpEff', 'print' : printSnpEff, 'walltime' : '10:00:00', 'pool' : 'primary'},
		'BwaAln' : {'name' : 'BwaAln', 'print' : printBwaAln, 'walltime' : '47:00:00', 'pool' : 'one'},
		'BwaMem' : {'name' : 'BwaMem', 'print' : printBwaMem, 'walltime' : '47:00:00', 'pool' : 'one'},
		'PlatypusGerm' : {'name' : 'PlatypusGerm', 'print' : printPlatypusGerm, 'walltime' : '15:00:00', 'pool' : 'primary'},
		'PlatypusPaired' : {'name' : 'PlatypusGerm', 'print' : printPlatypusPaired, 'walltime' : '15:00:00', 'pool' : 'primary'},
		'Mutect' : {'name' : 'Mutect', 'print' : printMutect, 'walltime' : '20:00:00', 'pool' : 'primary'},
		'MpileupPaired' : {'name' : 'MpileupPaired', 'print' : printMpileupPaired, 'walltime' : '20:00:00', 'pool' : 'primary'},
		'HaplotypeCaller' : {'name' : 'HaplotypeCaller', 'print' : printHaplotypeCaller, 'walltime' : '15:00:00', 'pool' : 'RAM'},
		'ScalpelGerm' : {'name' : 'ScalpelGerm', 'print' : printScalpelGerm, 'walltime' : '20:00:00', 'pool' : 'RAM'},
		'ScalpelPaired' : {'name' : 'ScalpelPaired', 'print' : printScalpelPaired, 'walltime' : '20:00:00', 'pool' : 'RAM'},
		'Varscan' : {'name' : 'Varscan', 'print' : printVarscan, 'walltime' : '20:00:00', 'pool' : 'primary'},
		'Strelka' : {'name' : 'Strelka', 'print' : printStrelka, 'walltime' : '20:00:00', 'pool' : 'RAM'},
		'SamtoolsFlagstat' : {'name' : 'SamtoolsFlagstat', 'print' : printSamtoolsFlagstat, 'exclusion' : None, 'walltime' : '06:00:00', 'pool' : 'primary'},
		'BedtoolsGenomeCoverage' : {'name' : 'BedtoolsGenomeCoverage', 'print' : printBedtoolsGenomeCoverage, 'exclusion' : None, 'walltime' : '08:00:00', 'pool' : 'primary'},
		'BamutilPerBaseCoverage' : {'name' : 'BamutilPerBaseCoverage', 'print' : printBamutilPerBaseCoverage, 'exclusion' : None, 'walltime' : '12:00:00', 'pool' : 'RAM'},
	}
	
	return d
	
def getBaseApps():
	""" Will also address run-type dependencies 
	
	'Processing', 'Genotyping', 'Processing_and_Genotyping'
	
	Need merge wrappers"""
	
	d = {'GatkBqsrGrpReduce' : {'name' : 'GatkBqsrGrpReduce', 'print' : printGatkBqsrGrpReduce, 'exclusion' : None, 'walltime' : '03:00:00', 'pool' : 'primary'},
		'SamtoolsParseContig' : {'name' : 'SamtoolsParseContig', 'print' : printSamtoolsParseContig, 'exclusion' : None, 'walltime' : '24:00:00', 'pool' : 'IO'},
		'SamtoolsExtractRg' : {'name' : 'SamtoolsExtractRg', 'print' : printSamtoolsExtractRg, 'exclusion' : None, 'walltime' : '24:00:00', 'pool' : 'IO'},
		'ConcatVcf' : {'name' : 'ConcatVcf', 'print' : printConcatVcf, 'exclusion' : None, 'walltime' : '10:00:00', 'pool' : 'primary'},
		'RgMergeSort' : {'name' : 'RgMergeSort', 'print' : printRgMergeSort, 'exclusion' : None, 'walltime' : '24:00:00', 'pool' : 'one'},
		'IndexBam' : {'name' : 'IndexBam', 'print' : printIndexBam, 'exclusion' : None, 'walltime' : '08:00:00', 'pool' : 'IO'},
		'ContigMergeSort' : {'name' : 'ContigMergeSort', 'print' : printContigMergeSort, 'exclusion' : None, 'walltime' : '24:00:00', 'pool' : 'one'}
	}
	
	return d
	
