import os, sys

def getHelpString():
	""" Default atring that should be printered out for exceptions. Will
	reference the wiki as well as the SwiftSeq mailing list"""
	
	helpStr = ('\n### Help ###\n'
				'Please carefully review the above exception\n'
				'For assistance running SwiftSeq, visit X. If your ' 
				'question remains unanswered, email us at Y.')
	return helpStr
