import os, sys
from swiftStructs import *
from swiftApps import *
from swiftSyntax import *

### This will be necessary for ASCAT (not necessarily) and tranlocations
### with Delly

### TO-DO - handle indexes in the cases of no alignment

def tumorNormalSwift(workflow, workDir, contigsFileFp, outDir):
	''' Perhaps break this up at some point '''
	
	
	##########################################################################
	# Stand alone testing - These will need to be specified via run or config
	##########################################################################
	#!# Will need to ensure these are the proper names and naming conventions we need
	# Maybe merge these for germline and tumor normal pairs
	
	refDir = workDir + '/' + outDir + '/' + 'Reference'
	sampleDataFile = 'samples.txt' # May not be needed in germline
	patientDataFile = workDir + '/' + outDir + '/individuals.txt'
	RGfiles = 'RGfiles.txt'
	sortApp = 'RgMergeSort' # This will need to be altered
	pairedAnalysisDir = 'pairedAnalyses'
	
	aligner = workflow.aligner
	# Both of these need to be arrays
	genotypers = workflow.genotypers
	structVarCallers = workflow.structVarCallers
	bamQualityControlApps = workflow.bamQualityControlApps
	
	
	alignment = workflow.alignment
	gatk = workflow.gatk
	genotyping = workflow.genotyping
	rmDup = workflow.rmDup
	structVars = workflow.structVars
	
	FP = workDir + '/SwiftSeq.swift'
	FH = open(FP, 'w')
	
	
	########################################################################
	## Print out Swift syntax 
	########################################################################
	# print the apps to swift file
	printSwiftApps(FH)
	# write custom structs to file
	# True indicates a paired run
	printCustomStructs(FH, True)
	
	# Map Strelka config
	if 'Strelka' in genotypers:
		print >>FH, 'file strelkaConfig <single_file_mapper; file="' + outDir + '/strelkaConfig.ini">;\n'
	
	printPairedSetup(FH, 0, contigsFileFp, patientDataFile, sampleDataFile)
	
	
	########################################################################
	## Aligment 
	########################################################################
	# inBam will be passed to split by contig if alignment is not needed
	inBam = printPreAlignment(FH, 3, RGfiles, alignment)
	
	if alignment:
		## Aln read groups & merge-sort
		printAlignment(FH, 3, aligner, sortApp)
		contigSplitBam = 'alnSampleBam' # Default bam name post-alignment
	else:
		contigSplitBam = inBam
		
	
	########################################################################
	##   Handle contigs   
	########################################################################
	if gatk:
		printGrpFilenames(FH, 3) # tab count of 1
		
	# Dup removal optional for non-alignment cases
	if not alignment:
		rmDup = False
	else:
		rmDup = True
		splitBam = ''
		
	
	########################################################################
	## Split into contig & rm dup per chr
	########################################################################
	# This step will always occur
	# The difference will be if dup removal is performed
	##!!!! If not alignement index inbam !!! ###
	genoBam = printContigSetup(FH, 3, contigSplitBam, contigSplitBam + 'Index', rmDup)
			
			
	########################################################################
	## GATK Post-processing 
	########################################################################
	# These steps will occur in a block
	# Will update the name of the genoBam if gatk performed
	if gatk:
		genoBam = printGatkAppCalls(FH, 4, genoBam)
	
	
	########################################################################
	# Assign contig bams to arrays 
	########################################################################
	index = 'contigName'		
	printPairedBamArrayAssignment(FH, 4, index, genoBam)
	closeBracket(FH, 3, '# End of contigs')
	
	
	########################################################################
	## Reduce bam steps - Sample level operation
	########################################################################
	# All reduce steps look for the no_mapped_reads flag within each wrapper
		
	# Create the command that will print the grp reduce call if GATK required
	if gatk:
		printReduceGrpAppCall(FH, 3)
	
	#!# This will be a disparity between germline and tumor-normal
	#!# Is germ conting merge sort ok here???
	
	# Should occur regardless of genotyping or not
	QCBam = printPairedContigMergeSort(FH, 3, 'geno', 'sample.dir', 'sample.ID')
	
	# Do QC here - shouldn't this use QCBam?
	# if bamQualityCOntrolApps is empty it will print nothing to the Swift script
	printQualityControl(FH, 3, 'pairedSampleData.wholeBam', 'sample.dir', 'sample.ID', bamQualityControlApps)
	
	printAssignPairedData(FH, 3)
	
	closeBracket(FH, 2, '# End of sample')
	closeBracket(FH, 1, '# End of tissue')
	
	
	##########################
	## Do paired genotyping
	##########################
	if genotyping:
		printPairedGeno(FH, 1, pairedAnalysisDir, refDir, genotypers, structVarCallers)
	
	closeBracket(FH, 0, '# End of patient/individual')
	
	FH.close()
	return FP


########################################################################
