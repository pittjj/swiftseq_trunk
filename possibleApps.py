
''' Alll program names must be the name of the apps as well as the output
name from the GUI '''

def possibleGatkPostPrograms():
	''' aaa '''

	Set = set(['GatkIndelRealignment', 'GatkBqsr'])
	return Set

def getPossibleAligners():
	''' aaa '''

	Set = set(['BwaAln', 'BwaMem', 'Bowtie2'])
	return Set

def possibleGenotypers():
	''' aaa '''
	
	Set = set(['PlatypusPaired', 'PlatypusGerm', 'HaplotypeCaller', 'Mutect', \
		'MpileupPaired', 'UnifiedGenotyper', 'ScalpelPaired', 'ScalpelGerm' \
		'Strelka', 'Varscan'])
	return Set

def possibleStructVariantCallers():
	''' aaa '''
	
	Set = set(['DellyGerm', 'DellyPaired', 'LumpyGerm', 'LumpyPaired'])
	return Set

def possibleRmDup():
	''' aaa '''
	
	Set = set(['PicardMarkDuplicates'])
	return Set
	
def possibleBamQualityControl():
	''' aaa '''
	
	Set = set(['SamtoolsFlagstat', 'BedtoolsGenomeCoverage', 'BamutilPerBaseCoverage'])
	return Set
	
def allJavaPrograms():
	''' These should be programs that use "--param=value syntax in their arguments '''
	
	Set = set(['PicardMarkDuplicates'])
	return Set
	
def possibleRunTypes():
	''' aaa '''
	
	Set = set(['Processing', 'Genotyping', 'Processing_and_Genotyping'])
	return Set
	
def possibleDataTypes():
	''' aaa '''
	
	Set = set(['Germline', 'Tumor_Normal_Pair'])
	return Set
	
def possibleProgramTypes():
	''' aaa '''
	
	Set = set(['Aligner', 'Genotyper', 'Structural_Variant_Caller', \
		'Gatk_Post-processing', 'Duplicate_Removal', 'Bam_Quality_Control'])
	return Set
	
def possibleProgramParams():
	''' aaa '''
	
	Set = set(['walltime', 'params'])
	return Set
	
def allPossiblePrograms():
	''' Need to add more programs ... gatk.... bam qc... etc '''
	
	Set = possibleRmDup().add(possibleStructVariantCallers()).add(possibleGenotypers()).add(possibleAligners()).add(possibleBamQualityControl())
	return Set
	
