######################
# COMMENTS / To-do
######################
# Need to come back to the custom Swift structs and remove any variables
# not utilized


def printCustomStructs(FH, paired):
	""" These same structs will be printed for each Swift script"""
	
	if paired:
		sample = ('type Sample {\n'
				'\tstring ID;\n'
				'\tstring sampleDir;\n'
				'\tstring dir;\n'
				'\tstring filepath;\n'
			'}\n\n')
			
	else: # if germline
		sample = ('type Sample {\n'
				'\tstring ID;\n'
				'\tstring dir;\n'
			'}\n\n')
	
	Str = ('##################\n'
			'# Custom Structs #\n'
			'##################\n'
			
			'# ID is the name of the samples stripped of .bam\n'
			'# dir is the path to samples analysis directory\n'
			'# sampleDir is the name of directory that holds the input file (typically bam)\n\n'
			
			'type file;\n\n'
			
			'type Patient {\n'
				'\tstring patient;\n'
				'\tstring dir;\n'
			'}\n\n'
			
			+ sample +
			
			'# Will use an associative array for contigs\n'
			'type PairedSample {\n'
				'\tfile[string] contigBams;\n'
				'\tfile[string] contigBamsIndex;\n'
				'\tfile wholeBam;\n'
				'\tfile wholeBamIndex;\n'
				'\tstring ID;\n'
				'\tstring dir;\n'
				'\tstring sampleDir;\n'
			'}\n\n')
	
	FH.write(Str)	
	
