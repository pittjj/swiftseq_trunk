import os, sys
from swiftStructs import *
from swiftApps import *
from swiftSyntax import *

def germlineSwift(workflow, workDir, contigsFile, outDir):
	''' ggggg '''
	
	########################################################################
	# Stand alone testing - need to be specified via run or config
	########################################################################
	#!# Will need to ensure these are the proper names and naming conventions we need
	# Maybe merge these for germline and tumor normal pairs
	
	refDir = workDir + '/' + outDir + '/' + 'Reference'
	sampleDataFile = outDir + '/' + 'individuals.txt' # May not be needed in germline
	RGfiles = 'RGfiles.txt'
	sortApp = 'RgMergeSort' # This will need to be altered
	
	aligner = workflow.aligner
	# Both of these need to be arrays
	genotypers = workflow.genotypers
	structVarCallers = workflow.structVarCallers
	bamQualityControlApps = workflow.bamQualityControlApps
	
	alignment = workflow.alignment
	gatk = workflow.gatk
	genotyping = workflow.genotyping
	rmDup = workflow.rmDup
	structVars = workflow.structVars
	
	FP = workDir + '/SwiftSeq.swift'
	FH = open(FP, 'w')
	
	########################################################################
	## Print out Swift syntax for apps and custom structs
	########################################################################
	# print the apps to swift file
	printSwiftApps(FH)
	# write custom structs to file
	# False indicates a germline run
	printCustomStructs(FH, False)
	
	########################################################################
	## Set up w/ sample for loop
	########################################################################
	# Will occur in all runs (alignment, geno, or both)
	printSetup(FH, 0, contigsFile, sampleDataFile)
	
	
	########################################################################
	## Aligment
	########################################################################
	# inBam will be passed to split by contig if alignment is not needed
	inBam = printPreAlignment(FH, 1, RGfiles, alignment)
	
	if alignment:
		## Aln read groups & merge-sort
		printAlignment(FH, 1, aligner, sortApp)
		contigSplitBam = 'alnSampleBam' # Default bam name post-alignment
	else:
		contigSplitBam = inBam
		
	
	########################################################################
	##   Handle contigs
	########################################################################
	if gatk:
		printGrpFilenames(FH, 1) # tab count of 1
		
	# For each caller print the auto increment array to hold vcfs
	if genotyping:
		for genotyper in genotypers:
			printVcfArray(FH, 1, genotyper) # tab count of 1
				
	if structVars:
		for structVarCaller in structVarCallers:
			printVcfArray(FH, 1, structVarCaller) # tab count of 1
	
	
	
	# Dup removal optional for non-alignment cases
	if not alignment:
		rmDup = False
	else:
		rmDup = True
	
			
	########################################################################
	## Split into contig & rm dup per chr
	########################################################################
	# This step will always occur
	# The difference will be if dup removal is performed
	genoBam = printContigSetup(FH, 1, contigSplitBam, contigSplitBam + 'Index', rmDup)
			
			
	########################################################################
	## GATK Post-processing 
	########################################################################
	# These steps will occur in a block
	# Will update the name of the genoBam if gatk performed
	if gatk:
		genoBam = printGatkAppCalls(FH, 2, genoBam)
	
			
	########################################################################
	## Single sample coordinate genotyping 
	########################################################################
	if genotyping:
		printSingleSampleGeno(FH, 2, genotypers, refDir, genoBam, genoBam + 'Bai')
			
			
	########################################################################
	# Structural variant calling 
	########################################################################
	# Step flexible even if Delly only supported
	if structVars:
		printDellyApp(FH, 2, genoBam, genoBam + 'Bai')	
			
	
	########################################################################
	# Assign contig bams to arrays 
	########################################################################
	index = 'contigName'		
	printGermBamArrayAssignment(FH, 2, index, genoBam, genoBam + 'Bai')
	closeBracket(FH, 1, '# End of contig')
	
	
	########################################################################
	## Reduce bam steps
	########################################################################
	# All reduce steps look for the no_mapped_reads flag within each wrapper
		
	# Create the command that will print the grp reduce call if GATK required
	if gatk:
		printReduceGrpAppCall(FH, 1)
		
	# Merge contigs and save the output bam variable name
	if alignment:
		QCBam = printContigMergeSort(FH, 1, 'contigBams', 'geno', 'sample.dir', 'sample.ID')
	else:
		# if only genotyping do QC on the input bam
		QCBam = inBam
		
	
	########################################################################
	## Reduce vcf steps 
	########################################################################
	# All vcfs will be merged here
		
	if genotyping:
		for genotyper in genotypers:
			printReduceVcfApp(FH, 1, genotyper, 'sample.dir', 'sample.ID')
		
	if structVars:
		for structVarCaller in structVarCallers:
			print structVarCaller
			
			# Will perform translocations analysis prior to the merge
			if structVarCaller == 'DellyGerm':
				printGermDellyTransApp(FH, 1, QCBam, QCBam + 'Index')
			
			printReduceVcfApp(FH, 1, structVarCaller, 'sample.dir', 'sample.ID')
		
	
	########################################################################
	## Merged geno bam operations 
	########################################################################
	## Quality control
	printQualityControl(FH, 1, QCBam, 'sample.dir', 'sample.ID',  bamQualityControlApps)
	closeBracket(FH, 0, '# End of sample') # This is the end of the sample
	
	FH.close() # closes the swift script file
	return FP
	
