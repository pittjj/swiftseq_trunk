#! /usr/bin/env python

import argparse, os, sys, subprocess, multiprocessing
from run import Run


# This arg parse will handle all of the details how the workflow runs
# Will not handle details of underlying algorithms (except num cores)
# This latter aspect will be handled by the json and the GUI

# Here we need to think about how to gather all variables required to run
# onBeagle and other locations
# Need to set
# -------------------
# LD_LIBRARY_PATH
# CLASSPATH (for gatk queue) $gatkQueue:$grpReduceDir
# ^ Force these to be our compiled version with queue... having this required
# will be tricky...Talk to Lorenzo about this
# /soft/python/2.7/2.7.3-vanilla/python/lib


"""
Arguments that need to be added to argparse

- restart

"""

def getHelpString():
	""" Default atring that should be printered out for exceptions. Will
	reference the wiki as well as the SwiftSeq mailing list"""
	
	helpStr = ('\n### Help ###\n'
				'Please carefully review the above exception\n'
				'For assistance running SwiftSeq, visit X. If your ' 
				'question remains unanswered, email us at Y.')
	return helpStr

def getEnviromentException(varName):
	""" The exception string that will be generated and expose to the user
	if a required environmental variable cannot be found"""
	
	exception = ('Environment variable "' + varName + '" does not exist. '
		'Please export ' + varName + '=<value> and re-execute SwiftSeq\n'
		+ getHelpString())
	return exception
	
def setEnvironmentVariables(varName, var):
	""" Set all variables for the current shell - Will be inherited by
	subprocesses"""
	
	os.environ[varName] = var
	
def executeSwift(swift, swiftConfig, swiftScript, runName):
	''' dddd '''
	
	os.system(swift + ' -config ' + swiftConfig + ' ' + swiftScript)
	
def printRestartConfigFile(swift, conf, swiftScript, SWIFT_HOME, SWIFT_USERHOME):
	''' sss '''
	
	FH = open('restart.conf', 'w')
	
	FH.write('swift=%s\n' % (swift))
	FH.write('conf=%s\n' % (conf))
	FH.write('swiftScript=%s\n' % (swiftScript))
	FH.write('SWIFT_HOME=%s\n' % (SWIFT_HOME))
	FH.write('SWIFT_USERHOME=%s\n' % (SWIFT_USERHOME))
	
	FH.close()
	
	

###########################
# Parser helper functions
###########################
def isValidFile(parser, arg):
	""" Will check that files exists and will raise a parser error if it
	does not. This will not yield true for directories"""
	
	if not os.path.isfile(arg):
		parser.error(('The provided file ' + arg + ' does not exist.'
		+ getHelpString()))
	else:
		return arg
        
def isValidDir(parser, arg):
	""" Will check that the path exists (is a directory) and also be sure
	that it isn't a file. Will raise a parser error if it is not."""
	
	if not os.path.exists(arg) or os.path.isfile(arg):
			parser.error(('The provided argument ' + arg + ' is not a directory.\n'
			+ getHelpString()))
	else:
		return arg
		
def isValidTmpDir(parser, arg):
	""" Will check that the path exists (is a directory) and also be sure
	that it isn't a file.
	- If the dir does not exist, will try to make it"""
	
	# if the path does not exist or is a file
	if not os.path.exists(arg) or os.path.isfile(arg):
		try:
			# Note: If a file is supplied a dir with the same path as that file
			# Will be created
			os.mkdir(arg)
		except OSError:
			parser.error(('The provided argument ' + arg + ' is not a directory and '
				'cannot be created.\n Please supply a valid dir or path.' + getHelpString()))
	
	# return the path of the arg, which will have existed or was created above
	return arg
	
################
## Main
################

# In this section should make passing of an xml optional

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--data', required=True, dest='data', type=lambda x: isValidDir(parser, x),
	help='The data directory containing sequencing data to be run (Required).')
parser.add_argument('--runName', type=str, dest='runName', default='SwiftSeq', nargs='?', const='all', 
	help='The name of the run (Default = SwiftSeq).')
# If this string is not a dir we should give a warning
# Look for this dir... if it doesn't exist, make it... if it can't be made throw error
parser.add_argument('--tmp', dest='tmp', type=lambda x: isValidTmpDir(parser, x),
	default=(os.environ['TMPDIR']), nargs='?', const='all', 
	help='Temporary directory for this run. Will be created if does not exist (Default = $TMPDIR).')
parser.add_argument('--foreachMax', type=float, dest='foreachMax', default=300, 
	help='The maximum number of simultaneous iterations per for loop in Swift (Default = 300).')
parser.add_argument('-n', '--numNodes', dest='numNodes', required=True, 
	help='The number of worker nodes (per pool) available to SwiftSeq for processing (Required).')
parser.add_argument('-c', '--coresPerNode', type=int, dest='coresPerNode', 
	default=(multiprocessing.cpu_count()), nargs='?', const='all', 
	help='Number of processing cores available per worker node (Default = <num cores on head node>).')
parser.add_argument('-t', '--tasksPerNode', type=int, dest='tasksPerNode', 
	default=8, nargs='?', const='all', 
	help='Number of tasks to pack (run simultaneously) on each worker node (Default = 8).')
parser.add_argument('-r', '--ramPerNode', type=int, dest='ramPerNode', 
	default=29000, nargs='?', const='all', 
	help='Amount of RAM available on each worker node (Default = 29000).')
parser.add_argument('-w', '--workflow', required=True, dest='workflowPath', type=lambda x: isValidFile(parser, x),
	help='The SwiftSeq workflow (in .json format) that will be run over input data (Required).')
parser.add_argument('--heapMax', type=int, dest='heapMax', default=2500, nargs='?', const='all', 
	help='Java max heap size (in MB) for the Swift process running on the headnode (Default = 2500)')
parser.add_argument('--gcThreads', type=int, dest='gcThreads', default=1, nargs='?', const='all', 
	help='Number of threads dedicated for parallel garbage collection on the Swift process (Default = 1)')
parser.add_argument('--jobTime', type=str, dest='jobTime', default='48:00:00', nargs='?', const='all',
	help='The maximum walltime (hh:mm:ss format) for a given Swift coaster block i.e. the max job time (Default = 48:00:00).')
parser.add_argument('--maxIO', type=int, dest='maxIO', default=200, nargs='?', const='all',
	help='The maximum number of IO intensive processes to run concurrently (Default = 200).')
parser.add_argument('--refConfig', required=True, dest='refConfig', type=lambda x: isValidFile(parser, x),
	help='Configuration file that contains paths to required reference files (Required).')
parser.add_argument('--exeConfig', required=True, dest='exeConfig', type=lambda x: isValidFile(parser, x),
	help='Configuration file that contains paths algorithm executables (Required).')
parser.add_argument('--retries', type=int, dest='retries', default=1, nargs='?', const='all', 
	help='The number of times an app should be re-executed after a failure (Default = 2)')
parser.add_argument('--project', type=str, dest='project', default=None, nargs='?', const='all',
	help='Project ID used to submit and run jobs through a scheduler (Default = None).')
parser.add_argument('--jobManager', type=str, required=True, dest='jobManager', default=None, nargs='?', const='all',
	help='Software used to submit and manage jobs (e.g. pbs, slurm, sge, etc.) (Required).')
parser.add_argument('--jobOptions', type=str, dest='jobOptions', default=None, nargs='?', const='all',
	help=('String that will be provided to jobOptions portion of the swift json config file (Default = None).\n'
	'This string should be a ; separated list of job options.'))
parser.add_argument('--queue', type=str, dest='queue', default=None, nargs='?', const='all',
	help='Queue that will be used by the jobManager for job submissions (Default = None).')
parser.add_argument('--lazyErrors', type=str, dest='lazyErrors', default=True, nargs='?', const='all',
	help=('If if a task fails have SwiftSeq continue with running remaining independent tasks. \n'
	'For any SwiftSeq runs with > 1 sample "True" is recommended (Default = True).'))
parser.add_argument('--workerTmp', type=bool, dest='workerTmp', default=False, nargs='?', const='all',
	help=('If certain wrappers should use /tmp on the working as tmp space. \n'
	'Should assist in reducing overall IO burden (Default = False).'))
parser.add_argument('--genoSegmentSize', type=int, dest='genoSegmentSize', default=10000000, nargs='?', const='all',
	help='Size of the segment used by any variant calling process (Default = 10000000).')

args = parser.parse_args()


# Get SwiftSeq.py executable directory
# Maybe put this in a try-except block
# Will strip the quotes returned by os.path.dirname
swiftSeqDir = repr(os.path.dirname(os.path.realpath(sys.argv[0])))[1:-1] + '/'
workDir = os.getcwd()
subprocess.call(swiftSeqDir +'/graphic.sh', shell=True)
print '\nPreparing Run...\n\n'

# logging dir
workerLoggingDir = workDir + '/' + 'workerLogging'
if not os.path.exists(workerLoggingDir):
    os.makedirs(workerLoggingDir)


##################################
# Gather enviroment variables
##################################
# PATH will be passed to each wrapper
try:
	PATH = os.environ['PATH']
except KeyError:
	exception = getEnviromentException('PATH')
	raise Exception(exception)

# SWIFTHOME
try:
	SWIFT_HOME = os.environ['SWIFT_HOME']
except KeyError:
	exception = getEnviromentException('SWIFT_HOME')
	raise Exception(exception)
	
# SWIFT_USERHOME
try:
	SWIFT_HOME = os.environ['SWIFT_USERHOME']
except KeyError:
	exception = getEnviromentException('SWIFT_USERHOME')
	raise Exception(exception)
	
# LD_LIBRARY_PATH
try:
	LD_LIBRARY_PATH = os.environ['LD_LIBRARY_PATH']
except KeyError:
	exception = getEnviromentException('LD_LIBRARY_PATH')
	raise Exception(exception)
	
##################################
# Set enviroment variables
##################################
# SWIFT_HEAP_MAX
SWIFT_HEAP_MAX = '%sM' % (str(args.heapMax))
setEnvironmentVariables('SWIFT_HEAP_MAX', SWIFT_HEAP_MAX)
#COG_OPTS
COG_OPTS = '-XX:+UseParallelGC -XX:ParallelGCThreads=%s' % (str(args.gcThreads))
setEnvironmentVariables('COG_OPTS', COG_OPTS)

#########################################
# Make instance of run class
#########################################
# Will set up all variables and wrappers
run = Run(args, PATH, workDir, swiftSeqDir, LD_LIBRARY_PATH)

# Write config file (for restarts)
printRestartConfigFile(run.exe['swift'],run.conf.swiftConf, \
 run.swiftScript, os.environ['SWIFT_HOME'], os.environ['SWIFT_USERHOME'])

# Execute Swift command
executeSwift(run.exe['swift'], run.conf.swiftConf, run.swiftScript, run.runName)
